## Project Description

This project encapsulates a simple back end server and front end portal with simple authentication.

To Setup the project, clone the repo & run ```yarn install``` to download all the dependencies:
If you dont have yarn installed in WSL follow instructions here: https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable

## Initial Setup
After setting up yarn and running the yarn install command, next you want to run ```yarn build:web``` then run ```yarn build:server``` which will do the inital build of the server and web portal. You will run these commands from the root directory, you can see what these commands do in the package.json file in the root directory

## Description of file structure
The folders are setup in 3 sections, the root level, server folder, and web folder.
The root level contains files and packages specifically to build the server and web portal docker containers.
The Server folder contains files and packages specifically for the server side of the application.
The Web folder contains files and packages specifically for the web portal side of the application.

## Start The Server

First start the database docker container locally by running: (Requires Docker to be installed and setup for WSL2)
### `yarn db:start`

To Start the backend server for development with hot-reloading run the following commands in 2 seperate terminals which will start the server on localhost:3001:
### `yarn build:watch` and `yarn start:watch`


## Start the Web Portal

Open a new terminal and run the following command to serve the react website with hot-reloading on [http://localhost:3000](http://localhost:3000)
### `yarn web`


## Installing Dependencies

When installing dependencies you need to change directories into the 'server' or the 'web' folders before using yarn to add the dependency. This way it will only add the depedency to the appropiate server or web folder.

## Database Structure and Adding to Database

This project uses the Knex package for managing the database. Knex has a concept called "migrations" which handles the incremental changes to the database structure such as adding tables, adding columns, etc.
The migrations folder is located at ./server/src/lib/migrations

You will note that each migration has a number associated with it, the number is a timestamp so it knows which migration to run first. For example if you have a users table and user_types table which are related. You need to create the user_types table before the user table because a user table column references the id of the user_types table.

To create new migrations you can run the following from the root directory ```yarn migration:create {name-your-migration-action}```
Usually you'd want to name your migrations based on what they are doing such as "add-phonenumber-column-to-user-table"

You can connect to your database with various apps that connect to postgres database's such as "dbeaver". When connecting to the dev database, all your preset credentials are located in the .app-config.yml and .app-config.secrets.yml files. Its best practice to keep the root level and the server directory app-config files in sync.

Documentation on app config is located at: https://app-config.dev/

## Database Seeds

In the ./server/seeds folder you will see some files which are initial entries into the database called "seeds". Sometimes there are times where you need to pre-populate your database with data such as user-types, etc.

In the seeds/user.yml file you will see we pre-setup a default user with the email: user@example.com and password: Test123!
The password field is hashed so the value stored in the database isnt plain text password, instead it is the hash of the Test123! password.
