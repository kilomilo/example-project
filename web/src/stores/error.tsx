import create from "zustand";

export enum ErrorType {
  Info = "info",
  Warning = "warning",
  Error = "error",
  Success = "success",
}

interface ErrorDetails {
  message: string;
  errorType: ErrorType;
}

interface ErrorStoreType {
  showError: boolean;
  errorDetails?: ErrorDetails;
  setShowError: (ErrorDetails: ErrorDetails) => void;
  closeError: () => void;
}

export const [useErrorStore, ErrorStore] = create<ErrorStoreType>((set) => ({
  showError: false,
  errorDetails: undefined,
  setShowError: (errorDetails: ErrorDetails) =>
    set(() => ({ showError: true, errorDetails })),
  closeError: () => set(() => ({ showError: false, Error: undefined })),
}));
