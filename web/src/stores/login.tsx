import create from "zustand";
import { apiGetRequest } from "../lib/apiRequest";

export interface User {
  id: number;
  insertDatetime: Date;
  fkUserTypeId: number;
  email: string;
  isActive: boolean;
  firstname: string;
  lastname: string;
  cellNumber: string;
  shopName: string | null;
  address: string | null;
  postal: string | null;
  city: string | null;
  province: string | null;
}

interface Login {
  user?: User;
  loggedIn: boolean;
  profileSetup?: boolean;
  profileSetupComplete: () => void;
  setUser: (newUser: User) => void;
  resetUser: () => void;
  logout: () => void;
  verifyUser: () => Promise<boolean>;
}

const verifyLogin = async () => {
  const response = await apiGetRequest('/auth/verify')
  return response.data.user;
}

export const [useLoginStore, loginStore] = create<Login>((set) => ({
  user: undefined,
  loggedIn: false,
  setUser: (newUser: User) => set(() => ({ user: newUser, loggedIn: true, profileSetup: true })),
  profileSetupComplete: () => set(() => ({ profileSetup: true })),
  resetUser: () => set(() => ({ user: undefined, loggedIn: false })),
  logout: () => {
    localStorage.clear();
    set(() => ({ user: undefined, loggedIn: false }));
  },
  verifyUser: async () => {
    try {
      const userDetails = await verifyLogin();
      set(() => ({ user: userDetails, loggedIn: true, profileSetup: true }));
      return true;
    } catch (err) {
      set(() => ({ user: undefined, loggedIn: false }));
      return false;
    }
  }
}));
