export const getAuthToken = () => {
  const token = localStorage.getItem('authToken');
  if (!token) {
    return ''
  }
  return token;
};

export const setAuthToken = (token: string) => {
  try {
    localStorage.setItem('authToken', token);
  } catch (err) {
    throw err;
  }
};
