import axios from 'axios';
import { setAuthToken, getAuthToken } from './auth';

const getApiHost = () => {
  return process.env.NODE_ENV === 'development' ? 'http://localhost:3001/api' : 'https://api.branchoutdev.com/api';
}

export const apiPostRequest = async (endpoint: string, data: object) => {
  const request = await axios.post(`${getApiHost()}${endpoint}`, {
    ...data
  }, {
    headers: {
      Authorization: `Bearer ${getAuthToken()}`,
    },
  });

  if (request.status === 401) {
    setAuthToken('');
    console.log('Recieved 401');
  }

  return request.data;
}

export const apiGetRequest = async (endpoint: string, queryParamsObject: object = {}) => {
  const request = await axios.get(`${getApiHost()}${endpoint}`, {
    headers: {
      Authorization: `Bearer ${getAuthToken()}`,
    },
    params: queryParamsObject || undefined,
  });

  if (request.status === 401) {
    setAuthToken('');
    console.log('Recieved 401');
  }

  return request.data;
}
