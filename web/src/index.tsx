import React from 'react';
import { render } from 'react-dom';
import App from './containers/index';
import '@fortawesome/fontawesome-free/js/brands'
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free';

const root = React.createElement(App, null, null);

render(root, document.getElementById('root'));
