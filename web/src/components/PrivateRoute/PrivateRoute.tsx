import React, { FC } from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import { useLoginStore } from "../../stores/login";

interface Props extends RouteProps {
  component: any;
}

const PrivateRoute: FC<Props> = ({ component: Component, ...rest }) => {
  const { loggedIn } = useLoginStore();
  return (
    <Route
      {...rest}
      render={(props) =>
        loggedIn ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
};

export default PrivateRoute;
