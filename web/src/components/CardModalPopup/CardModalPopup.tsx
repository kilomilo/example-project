import React, { FC, useState } from "react";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { apiPostRequest } from '../../lib/apiRequest';
import "./CardModalPopup.less";

export enum CardModalType {
  NewAppointment = "new-appointment",
  ConsultRequested = "consult-requested",
  ConsultBooked = "consult-booked",
  DepositReceived = "deposit-received",
  AppointmentBooked = "appointment-booked",
  UnavailableTime = "unavailable-time",
}

interface Props {
  cardModalOpen: boolean;
  cardModalOnClose: () => void;
  cardModalType: CardModalType;
  handleSave: () => void;
  handleClose: () => void;
}

interface AppointmentValues {
  fullname: string;
  cellNumber: string;
  email: string;
  requireConsult: boolean;
  consultStart: string | Date;
  consultEnd: string | Date;
  isVideoConsult: boolean;
  videoConsultUrl: string | null;
  appointmentStart: string | Date;
  appointmentEnd: string | Date;
}

const CardModalPopup: FC<Props> = ({
  cardModalOpen,
  cardModalType,
  cardModalOnClose,
  handleSave,
  handleClose,
}) => {
  const [notes, setNotes] = useState<string>("");
  const [appointmentArtist, setAppointmentArtist] = useState<string>("");
  const [appointmentValues, setAppointmentValues] = useState<AppointmentValues>(
    {
      fullname: "",
      cellNumber: "",
      email: "",
      requireConsult: true,
      consultStart: `2021-08-24T00:00`,
      consultEnd: `2021-08-24T00:00`,
      isVideoConsult: false,
      videoConsultUrl: '',
      appointmentStart: `2021-08-24T00:00`,
      appointmentEnd: `2021-08-24T00:00`,
    }
  );

  const handleAppointmentValueChange = (
    inputField: string,
    value: string | Date | boolean | null
  ) => {
    switch (inputField) {
      case "fullname":
        setAppointmentValues({
          ...appointmentValues,
          fullname: value as string,
        });
        break;
      case "cellNumber":
        if (value && typeof value === 'string' && value.length < 11) {
          setAppointmentValues({
          ...appointmentValues,
          cellNumber: value as string,
        });
        }
        break;
      case "email":
        setAppointmentValues({
          ...appointmentValues,
          email: value as string,
        });
        break;
      case "requireConsult":
        const consultNewValue = value === 'yes'
        setAppointmentValues({
          ...appointmentValues,
          requireConsult: consultNewValue as boolean,
        });
        break;
      case "consultStart":
        setAppointmentValues({
          ...appointmentValues,
          consultStart: value as string,
        });
        break;
      case "consultEnd":
        setAppointmentValues({
          ...appointmentValues,
          consultEnd: value as string,
        });
        break;
      case "isVideoConsult":
        const videoConsultNewValue = value === 'yes'
        setAppointmentValues({
          ...appointmentValues,
          isVideoConsult: videoConsultNewValue as boolean,
        });
        break;
      case "videoConsultUrl":
        setAppointmentValues({
          ...appointmentValues,
          videoConsultUrl: value as string,
        });
        break;
      case "appointmentStart":
        setAppointmentValues({
          ...appointmentValues,
          appointmentStart: value as string,
        });
        break;
      case "appointmentEnd":
        setAppointmentValues({
          ...appointmentValues,
          appointmentEnd: value as string,
        });
        break;
    }
  };

  const handleNewAppointmentSave = () => {
    console.log(appointmentValues);
    const newAppointmentValues = {
      ...appointmentValues
    };

    Object.keys(newAppointmentValues).forEach((key: string) => {
      if (key === 'consultStart' || key === 'consultEnd' || key === 'appointmentStart' || key === 'appointmentEnd') {
        newAppointmentValues[key] = new Date(newAppointmentValues[key]).toISOString()
      }
    })

    console.log(newAppointmentValues);

    apiPostRequest('/calendar/add-appointment', newAppointmentValues)
    .then((addAppointmentResponse) => {
      console.log({
        addAppointmentResponse
      })
      handleSave();
    }).catch(error => {
      console.error(error);
    })
  }

  const newAppointmentModal = (
    <div className="new-appointment-modal-container">
      <div className="modal-content-container">
        <div className="left-side-modal-content">
          <div className="modal-title-label">New Appointment</div>
          <div className="modal-form-wrapper">
            <form className="appointment-request-form" noValidate>
              <div className="appointment-details input-field hide">
                <div className="appointment-label">Artist:</div>
                <Select
                  labelId="appointment-artist-select"
                  id="demo-simple-select-outlined"
                  variant="outlined"
                  value={appointmentArtist}
                  onChange={(event) =>
                    setAppointmentArtist(event.target.value as string)
                  }
                  label="Artist"
                  fullWidth={true}
                >
                  <MenuItem value="">None</MenuItem>
                  <MenuItem value={"John Doe"}>John Doe</MenuItem>
                  <MenuItem value={"Alex Frey"}>Alex Frey</MenuItem>
                </Select>
                <Button
                  variant="contained"
                  className="view-details-button"
                  onClick={() => {}}
                >
                  View Artist
                </Button>
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Customer Name:</div>
                <TextField
                  id="outlined-basic"
                  label="Customer Name"
                  value={appointmentValues.fullname}
                  onChange={(event) => handleAppointmentValueChange('fullname', event.target.value)}
                  variant="outlined"
                  fullWidth={true}
                />
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Cell #:</div>
                <TextField
                  id="outlined-basic"
                  label="Cell #"
                  value={appointmentValues.cellNumber}
                  onChange={(event) => handleAppointmentValueChange('cellNumber', event.target.value)}
                  variant="outlined"
                  fullWidth={true}
                />
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Email:</div>
                <TextField
                  id="outlined-basic"
                  label="Email"
                  value={appointmentValues.email}
                  onChange={(event) => handleAppointmentValueChange('email', event.target.value)}
                  variant="outlined"
                  fullWidth={true}
                />
              </div>
              <div className="consult-required-wrapper input-field">
                <div className="side-by-side-together">
                  <div className="text-label">
                    Require Consult Before Appointment?
                  </div>
                  <Select
                    labelId="consult-required"
                    id="consult-required"
                    variant="outlined"
                    value={appointmentValues.requireConsult ? 'yes' : 'no'}
                    onChange={(event) => handleAppointmentValueChange('requireConsult', event.target.value as string)}
                  >
                    <MenuItem value={"yes"}>Yes</MenuItem>
                    <MenuItem value={"no"}>No</MenuItem>
                  </Select>
                </div>
                <div
                  className={`${
                    !appointmentValues.requireConsult ? "hide" : undefined
                  } consult-required-input`}
                >
                  <div className="side-by-side">
                    <TextField
                      id="datetime-local"
                      label="Consult Start"
                      type="datetime-local"
                      // defaultValue="2021-04-27T15:30"
                      value={appointmentValues.consultStart}
                      onChange={(event) => handleAppointmentValueChange('consultStart', event.target.value)}
                      className="input-field datetime-field"
                      size="medium"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    <TextField
                      id="datetime-local"
                      label="Consult End"
                      type="datetime-local"
                      // defaultValue="2021-04-27T15:30"
                      value={appointmentValues.consultEnd}
                      onChange={(event) => handleAppointmentValueChange('consultEnd', event.target.value)}
                      className="input-field datetime-field"
                      size="medium"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </div>
                  <div className="video-consult-wrapper input-field">
                    <div className="side-by-side-together">
                      <div className="text-label">Is this a video consult?</div>
                      <Select
                        labelId="video-consult-requested"
                        id="video-consult-requested"
                        variant="outlined"
                        value={appointmentValues.isVideoConsult ? 'yes' : 'no'}
                        onChange={(event) => handleAppointmentValueChange('isVideoConsult', event.target.value as string)}
                      >
                        <MenuItem value={"yes"}>Yes</MenuItem>
                        <MenuItem value={"no"}>No</MenuItem>
                      </Select>
                    </div>
                    <div
                      className={`${
                        !appointmentValues.isVideoConsult ? "hide" : undefined
                      } video-consult-input`}
                    >
                      <TextField
                        id="outlined-basic"
                        label="Video Meeting URL"
                        variant="outlined"
                        value={appointmentValues.videoConsultUrl}
                        onChange={(event) => handleAppointmentValueChange('videoConsultUrl', event.target.value)}
                      />
                    </div>
                  </div>
                </div>
                <div
                  className={`${
                    appointmentValues.requireConsult ? "hide" : undefined
                  } appointment-date-input`}
                >
                  <div className="side-by-side">
                    <TextField
                      id="datetime-local"
                      label="Appointment Start"
                      type="datetime-local"
                      // defaultValue={new Date().toLocaleString()}
                      value={appointmentValues.appointmentStart}
                      onChange={(event) => handleAppointmentValueChange('appointmentStart', event.target.value)}
                      className="input-field datetime-field"
                      size="medium"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    <TextField
                      id="datetime-local"
                      label="Appointment End"
                      type="datetime-local"
                      // defaultValue={new Date().toLocaleString()}
                      value={appointmentValues.appointmentEnd}
                      onChange={(event) => handleAppointmentValueChange('appointmentEnd', event.target.value)}
                      className="input-field datetime-field"
                      size="medium"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="right-side-modal-content">
          <div className="images-container">
            <div className="side-by-side">
              <div className="images-label">Images</div>
              <Button
                variant="contained"
                className="upload-image-button"
                onClick={() => {}}
              >
                Upload Images
              </Button>
            </div>
            <div className="images-list"></div>
          </div>
          <div className="notes-container">
            <div className="notes-label">Notes</div>
            <div className="notes-input">
              <TextField
                className="input-field-notes"
                variant="outlined"
                label="Notes..."
                value={notes}
                onChange={(event) => setNotes(event.target.value)}
                error={false}
                multiline
                rows="2"
              />
              <Button
                variant="contained"
                color="primary"
                disabled={notes.length === 0}
                className="add-notes-button"
                onClick={() => {}}
              >
                Add Note
              </Button>
            </div>
            <div className="notes-list"></div>
          </div>
        </div>
      </div>
      <div className="modal-footer-container">
        <div className="button-wrapper">
          <Button onClick={handleClose} className="profile-stepper-back-button">
            Cancel
          </Button>
          <Button
            variant="contained"
            color="primary"
            className="save-button"
            onClick={handleNewAppointmentSave}
          >
            Save
          </Button>
        </div>
      </div>
    </div>
  );

  const consultRequestedModal = (
    <div className="consult-requested-modal-container">
      <div className="modal-content-container">
        <div className="left-side-modal-content">
          <div className="modal-title-label">Consult Request</div>
          <div className="modal-form-wrapper">
            <form className="consult-request-form" noValidate>
              <div className="appointment-details input-field">
                <div className="appointment-label">Artist:</div>
                <div>John Doe</div>
                <Button
                  variant="contained"
                  className="view-details-button"
                  onClick={() => {}}
                >
                  View Artist
                </Button>
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Customer Name:</div>
                <div>Matt Gibson</div>
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Cell #:</div>
                <div>1-234-567-8910</div>
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Email:</div>
                <div>mgibson@gmail.com</div>
              </div>
              <div className="side-by-side">
                <TextField
                  id="datetime-local"
                  label="Consult Start"
                  type="datetime-local"
                  defaultValue="2021-04-27T15:30"
                  className="input-field datetime-field"
                  size="medium"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  id="datetime-local"
                  label="Consult End"
                  type="datetime-local"
                  defaultValue="2021-04-27T15:30"
                  className="input-field datetime-field"
                  size="medium"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </div>
              <div className="video-consult-wrapper input-field">
                <div className="side-by-side-together">
                  <Select
                    labelId="video-consult-requested"
                    id="video-consult-requested"
                    variant="outlined"
                    // value={videoConsult}
                    // onChange={(event) =>
                    //   setVideoConsult(event.target.value as string)
                    // }
                  >
                    <MenuItem value={"yes"}>Yes</MenuItem>
                    <MenuItem value={"no"}>No</MenuItem>
                  </Select>
                  <div className="text-label">Is this a video consult?</div>
                </div>
                <div
                  // className={`${
                  //   videoConsult === "no" ? "hide" : undefined
                  // } video-consult-input`}
                >
                  <TextField
                    id="outlined-basic"
                    label="Video Meeting URL"
                    variant="outlined"
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="right-side-modal-content">
          <div className="images-container">
            <div className="side-by-side">
              <div className="images-label">Images</div>
              <Button
                variant="contained"
                className="upload-image-button"
                onClick={() => {}}
              >
                Upload Images
              </Button>
            </div>
            <div className="images-list">
              <a href="/" target="_blank">
                sample.png
              </a>
              <a href="/" target="_blank">
                sample2.png
              </a>
            </div>
          </div>
          <div className="notes-container">
            <div className="notes-label">Notes</div>
            <div className="notes-input">
              <TextField
                className="input-field-notes"
                variant="outlined"
                label="Notes..."
                value={notes}
                onChange={(event) => setNotes(event.target.value)}
                error={false}
                multiline
                rows="2"
              />
              <Button
                variant="contained"
                color="primary"
                disabled={notes.length === 0}
                className="add-notes-button"
                onClick={() => {}}
              >
                Add Note
              </Button>
            </div>
            <div className="notes-list"></div>
          </div>
        </div>
      </div>
      <div className="modal-footer-container">
        <div className="button-wrapper">
          <Button onClick={handleClose} className="profile-stepper-back-button">
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleSave}>
            Save
          </Button>
        </div>
      </div>
    </div>
  );

  const consultBookedModal = (
    <div className="consult-requested-modal-container">
      <div className="modal-content-container">
        <div className="left-side-modal-content">
          <div className="modal-title-label">Consult Booked</div>
          <div className="modal-form-wrapper">
            <form className="consult-request-form" noValidate>
              <div className="appointment-details input-field">
                <div className="appointment-label">Artist:</div>
                <div>John Doe</div>
                <Button
                  variant="contained"
                  className="view-details-button"
                  onClick={() => {}}
                >
                  View Artist
                </Button>
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Customer Name:</div>
                <div>Matt Gibson</div>
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Cell #:</div>
                <div>1-234-567-8910</div>
              </div>
              <div className="appointment-details input-field">
                <div className="appointment-label">Email:</div>
                <div>mgibson@gmail.com</div>
              </div>
              <div className="side-by-side">
                <TextField
                  id="datetime-local"
                  label="Consult Start"
                  type="datetime-local"
                  defaultValue="2021-04-27T15:30"
                  className="input-field datetime-field"
                  size="medium"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  id="datetime-local"
                  label="Consult End"
                  type="datetime-local"
                  defaultValue="2021-04-27T15:30"
                  className="input-field datetime-field"
                  size="medium"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </div>
              <div className="video-consult-wrapper input-field">
                <div className="side-by-side-together">
                  <Select
                    labelId="video-consult-requested"
                    id="video-consult-requested"
                    variant="outlined"
                    // value={videoConsult}
                    // onChange={(event) =>
                    //   setVideoConsult(event.target.value as string)
                    // }
                  >
                    <MenuItem value={"yes"}>Yes</MenuItem>
                    <MenuItem value={"no"}>No</MenuItem>
                  </Select>
                  <div className="text-label">Is this a video consult?</div>
                </div>
                <div
                  // className={`${
                  //   videoConsult === "no" ? "hide" : undefined
                  // } video-consult-input`}
                >
                  <TextField
                    id="outlined-basic"
                    label="Video Meeting URL"
                    variant="outlined"
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="right-side-modal-content">
          <div className="images-container">
            <div className="side-by-side">
              <div className="images-label">Images</div>
              <Button
                variant="contained"
                className="upload-image-button"
                onClick={() => {}}
              >
                Upload Images
              </Button>
            </div>
            <div className="images-list">
              <a href="/" target="_blank">
                sample.png
              </a>
              <a href="/" target="_blank">
                sample2.png
              </a>
            </div>
          </div>
          <div className="notes-container">
            <div className="notes-label">Notes</div>
            <div className="notes-input">
              <TextField
                className="input-field-notes"
                variant="outlined"
                label="Notes..."
                value={notes}
                onChange={(event) => setNotes(event.target.value)}
                error={false}
                multiline
                rows="2"
              />
              <Button
                variant="contained"
                color="primary"
                disabled={notes.length === 0}
                className="add-notes-button"
                onClick={() => {}}
              >
                Add Note
              </Button>
            </div>
            <div className="notes-list"></div>
          </div>
        </div>
      </div>
      <div className="modal-footer-container">
        <div className="button-wrapper">
          <Button onClick={handleClose} className="profile-stepper-back-button">
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleSave}>
            Save
          </Button>
        </div>
      </div>
    </div>
  );

  const unavailableTimeModal = (
    <div className="unavailable-time-modal-container">
      <div className="modal-content-container">
        <div className="modal-title-label">Add Unavailable Time</div>
        <div className="modal-form-wrapper">
          <form className="unavailable-time-form" noValidate>
            <div className="side-by-side">
              <TextField
                id="datetime-local"
                label="Unavailable Start"
                type="datetime-local"
                defaultValue="2021-04-27T15:30"
                className="input-field datetime-field"
                size="medium"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="datetime-local"
                label="Unavailable End"
                type="datetime-local"
                defaultValue="2021-04-27T15:30"
                className="input-field datetime-field"
                size="medium"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
          </form>
        </div>
      </div>
      <div className="modal-footer-container">
        <div className="button-wrapper">
          <Button onClick={handleClose} className="profile-stepper-back-button">
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={handleSave}>
            Save
          </Button>
        </div>
      </div>
    </div>
  );

  let modalBody: any;

  switch (cardModalType) {
    case CardModalType.NewAppointment:
      modalBody = newAppointmentModal;
      break;
    case CardModalType.ConsultRequested:
      modalBody = consultRequestedModal;
      break;
    case CardModalType.ConsultBooked:
      modalBody = consultBookedModal;
      break;
    case CardModalType.UnavailableTime:
      modalBody = unavailableTimeModal;
      break;
  }

  return (
    <div>
      <Modal
        open={cardModalOpen}
        onClose={cardModalOnClose}
        aria-labelledby="card-modal-title"
        aria-describedby="card-modal-description"
        className="card-modal-popup"
      >
        {modalBody}
      </Modal>
    </div>
  );
};

export default CardModalPopup;
