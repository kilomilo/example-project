import React, { FC } from "react";
import Loader from "react-loader-spinner";
import "./Loading.less";

interface Props {
  show: boolean;
}

const Loading: FC<Props> = ({ show }) => {
  return (
    <div className={`loading-container ${show ? 'show' : ''}`}>
      <div className="logo">
        <img
          src={`${process.env.PUBLIC_URL}/img/logo-placeholder.png`}
          alt="Logo"
        />
      </div>
      <div className="spinner-container">
        <h1>Loading</h1>
        <Loader
          type="Circles"
          color="#2b2b2b"
          height={100}
          width={100}
        />
      </div>
    </div>
  );
};

export default Loading;
