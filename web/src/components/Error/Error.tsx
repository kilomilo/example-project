import React, { FC } from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { useErrorStore } from "../../stores/error";

interface Props {}

function Alert(props: any) {
  return <MuiAlert elevation={1} variant="filled" {...props} />;
}

const ErrorPopup: FC<Props> = () => {
  const { showError, errorDetails, closeError } = useErrorStore();
  return (
    <Snackbar
      open={showError}
      autoHideDuration={10000}
      onClose={() => closeError()}
    >
      <Alert onClose={() => closeError()} severity={errorDetails && errorDetails.errorType}>
        {errorDetails && errorDetails.message}
      </Alert>
    </Snackbar>
  );
};

export default ErrorPopup;
