import React, { FC, useState, useEffect } from "react";
import "./login.less";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import { apiPostRequest } from "../../lib/apiRequest";
import { useLoginStore } from "../../stores/login";
import { setAuthToken } from "../../lib/auth";
import { useErrorStore, ErrorType } from "../../stores/error";

interface Props {}

enum LoginModalType {
  Login = "login",
  Apply = "apply",
}

const LoginScreen: FC<Props> = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showIncorrectError, setShowIncorrectError] = useState(false);
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [loginModalType, setLoginModalType] = useState<LoginModalType>(
    LoginModalType.Login
  );
  const { setUser } = useLoginStore();
  const { setShowError } = useErrorStore();

  const handleLogin = () => {
    apiPostRequest("/auth/login", {
      email,
      password,
    })
      .then((responseData) => {
        console.log(responseData);
        if (responseData.success) {
          setAuthToken(responseData.data.token);
          setUser(responseData.data.user);
        }
      })
      .catch((err) => {
        setShowError({
          message: "Incorrect email or password",
          errorType: ErrorType.Error,
        });
        setShowIncorrectError(true);
      });
  };

  useEffect(() => {
    const listener = (event: any) => {
      if (event.code === "Enter" || event.code === "NumpadEnter") {
        handleLogin();
      }
    };
    document.addEventListener("keydown", listener);
    return () => {
      document.removeEventListener("keydown", listener);
    };
    // eslint-disable-next-line
  }, [email, password]);

  const handleEmailChange = (event: any) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event: any) => {
    setPassword(event.target.value);
  };

  const loginFormModal = (
    <div className="login-form">
      <div className="heading">Login</div>
      <FormControl variant="outlined">
        <TextField
          className="username-field"
          variant="outlined"
          id="outlined-textarea"
          label="Email"
          value={email}
          onChange={handleEmailChange}
          error={showIncorrectError}
          required
        />
      </FormControl>
      <FormControl variant="outlined">
        <TextField
          className="password-field"
          variant="outlined"
          id="outlined-textarea"
          label="Password"
          type="password"
          value={password}
          onChange={handlePasswordChange}
          error={showIncorrectError}
          required
        />
      </FormControl>
      <FormControl>
        <Button
          className="login-button"
          variant="contained"
          color="primary"
          disableElevation
          onClick={handleLogin}
          type="submit"
        >
          Login
        </Button>
      </FormControl>
    </div>
  );

  let modalBody: any;

  switch (loginModalType) {
    case LoginModalType.Login:
      modalBody = loginFormModal;
      break;
  }

  return (
    <div className="login-screen-container">
      <div className="logo-wrapper">
        <img
          src={`${process.env.PUBLIC_URL}/img/logo-placeholder.png`}
          alt="Logo"
        />
      </div>
      <div className="login-apply-buttons-wrapper">
        <Button
          variant="outlined"
          onClick={() => {
            setLoginModalType(LoginModalType.Login);
            setLoginModalOpen(true);
          }}
          className="login-modal-button login-button"
        >
          Login
        </Button>
      </div>
      <Modal
        open={loginModalOpen}
        onClose={() => setLoginModalOpen(false)}
        aria-labelledby="Login"
        aria-describedby="Please Select Login"
        className="login-modal-popup"
      >
        {modalBody}
      </Modal>
    </div>
  );
};

export default LoginScreen;
