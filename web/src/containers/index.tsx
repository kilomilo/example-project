import React, { FC, useEffect, useState } from "react";
import "./index.less";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import LoginScreen from "./login/login";
import Nav from "./nav/nav";
import Home from "./home/home";
import Loading from "../components/Loading/Loading";
import PrivateRoute from "../components/PrivateRoute/PrivateRoute";
import ErrorPopup from "../components/Error/Error";
import { useLoginStore } from "../stores/login";

interface Props {}

// const RedirectToLogin = () => <Redirect to="/login" />;

const App: FC<Props> = () => {
  const { user, loggedIn, verifyUser } = useLoginStore();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    verifyUser()
      .then((res) => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [loggedIn, verifyUser]);

  return (
    <>
      {loggedIn ? (
        <>
          <Router>
            {loggedIn ? <Nav /> : <></>}
            <ErrorPopup />
            <Switch>
              <Route exact path="/">
                {(loggedIn ? <Redirect to="/home" /> : <Redirect to="/settings" />)}
              </Route>
              <PrivateRoute component={Home} path="/home" exact />
              <Route path="*">
                <>The page you requested does not exist.</>
              </Route>
            </Switch>
          </Router>
        </>
      ) : (
        <LoginScreen />
      )}
      <Loading show={loading} />
    </>
  );
};

export default App;
