import React, { FC } from "react";
import "./home.less";

interface Props {}

const Home: FC<Props> = () => {


  return (
    <div className="home-container">
      <div className="header">
        <h1 className="header-label">Home</h1>
      </div>

      Home Page content
    </div>
  );
};

export default Home;
