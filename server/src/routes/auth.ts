import {
  bindRouteActions,
  bodyparser,
  HttpMethod,
  propagateErrors,
  propagateValues,
  route,
  RouteFactory,
  emptySchema,
  err,
} from "@lcdev/router";
import * as Knex from "knex";
import config from '@lcdev/app-config';
import { getKnex } from "../lib/database";
import { compare, hash, genSalt } from "bcrypt";
import { sign, verify } from "jsonwebtoken";

type Dependencies = {
  kx: Knex;
};

export interface DecodedJwt {
  id: number;
  insertDatetime: Date;
  fkUserTypeId: number;
  email: string;
  isActive: boolean;
  firstname: string;
  lastname: string;
  cellNumber: string;
  address: string | null;
  postal: string | null;
  city: string | null;
  province: string | null;
  iat: number;
  exp: number;
}

const factory: RouteFactory<Dependencies> = {
  prefix: "/auth",

  getDependencies() {
    return {
      kx: getKnex(),
    };
  },

  middleware: () => [propagateErrors(true), propagateValues(), bodyparser()],

  create(dependencies: Dependencies) {
    return bindRouteActions(dependencies, [
      route({
        path: "/login",
        method: HttpMethod.POST,
        schema: emptySchema()
          .addString("email", { maxLength: 100 }, true, false)
          .addString("password", { maxLength: 100 }, true, false),
        middleware: [],
        returning: {
          user: true,
          token: true,
        },
        async action(ctx, { email, password }) {
          if (!email || !password) {
            throw err(400, "Please provide a email and password.");
          }

          try {
            const [user] = await this.kx("user")
              .select("*")
              .where({ email })
              .limit(1);

            if (!user) {
              throw err(401, "Incorrect email or password.");
            }

            const compareResults = await compare(password, user.password)

            if (compareResults) {
              delete user.password;
              const token = sign(user, config.auth.secretKey, {
                expiresIn: '30d'
              });

              return {
                user,
                token,
              }
            }
            throw err(401, "Incorrect email or password.");
          } catch (error) {
            throw err(401, "Incorrect email or password.");
          }
        },
      }),
      route({
        path: "/register",
        method: HttpMethod.POST,
        schema: emptySchema()
          .addString("email", { maxLength: 100 }, true, false)
          .addString("password", { maxLength: 100 }, true, false)
          .addString("firstname", { maxLength: 100 }, true, false)
          .addString("lastname", { maxLength: 100 }, true, false)
          .addString("cell_number", { maxLength: 11 }, true, false),
        middleware: [],
        returning: {
          user: true,
        },
        async action(ctx, { email, password, firstname, lastname, cell_number }) {
          try {
            const [userEmailExists] = await this.kx('user')
            .select('email')
            .where({ email })
            .limit(1);

            if (userEmailExists) {
              throw err(400, 'Email address already exists')
            }

            const saltRounds = await genSalt(10);

            const hashedPassword = await hash(password, saltRounds);

            const [user] = await this.kx("user")
              .insert({
                email,
                password: hashedPassword,
                fkUserTypeId: 3,
                isActive: false,
                firstname,
                lastname,
                cell_number
              }, '*');

              delete user.password;
              return { user };
          } catch (error) {
            if (error.statusCode) {
              throw error;
            }
            throw err(500, "There was an error when trying to register the user.");
          }
        },
      }),
      route({
        path: "/verify",
        method: HttpMethod.GET,
        schema: emptySchema(),
        middleware: [],
        returning: {
          user: true,
        },
        async action(ctx) {
          try {
            const token = ctx.headers.authorization && ctx.headers.authorization.split(' ')[1]
            const decodedJwt = await verify(token, config.auth.secretKey);

            return { user: decodedJwt };
          } catch (error) {
            if (error.statusCode) {
              throw error;
            }
            throw err(401, "Unauthenicated.");
          }
        },
      }),
    ]);
  },
};

export default factory;
