import {
  bindRouteActions,
  bodyparser,
  HttpMethod,
  propagateErrors,
  propagateValues,
  route,
  RouteFactory,
  emptySchema,
  err,
} from "@lcdev/router";
import * as Knex from "knex";
import config from "@lcdev/app-config";
import { getKnex } from "../lib/database";
import { hash, genSalt } from "bcrypt";

type Dependencies = {
  kx: Knex;
};

const factory: RouteFactory<Dependencies> = {
  prefix: "/users",

  getDependencies() {
    return {
      kx: getKnex(),
    };
  },

  middleware: () => [propagateErrors(true), propagateValues(), bodyparser()],

  create(dependencies: Dependencies) {
    return bindRouteActions(dependencies, [
      route({
        path: "/get-users-list",
        method: HttpMethod.GET,
        schema: emptySchema(),
        middleware: [],
        returning: [
          {
            id: true,
            fkUserTypeId: true,
            fkSchedulerUserId: true,
            type: true,
            email: true,
            isActive: true,
            firstname: true,
            lastname: true,
            cellNumber: true,
            address: true,
            city: true,
            postal: true,
            province: true,
          },
        ],
        async action(ctx) {
          try {
            const userList = await this.kx("user")
              .innerJoin(
                "user_type",
                "user.fk_user_type_id",
                "=",
                "user_type.id"
              )
              .select([
                "user.id as id",
                "fkUserTypeId",
                "fkSchedulerUserId",
                "user_type.type as type",
                "email",
                "isActive",
                "firstname",
                "lastname",
                "cellNumber",
                "address",
                "city",
                "postal",
                "province",
              ]);

            return userList;
          } catch (error) {
            if (error.statusCode) {
              throw error;
            }
            throw err(500, "There was an error getting the list of users.");
          }
        },
      }),
      route({
        path: "/add-user",
        method: HttpMethod.POST,
        schema: emptySchema()
          .addNumber("userTypeId", {}, true, false)
          .addNumber("schedulerId", {}, false, true)
          .addString("email", { maxLength: 100 }, true, false)
          .addString("password", { maxLength: 100 }, true, false)
          .addString("firstname", { maxLength: 100 }, true, false)
          .addString("lastname", { maxLength: 100 }, true, false)
          .addString("cellNumber", { maxLength: 11 }, true, false),
        middleware: [],
        returning: {
          user: true,
        },
        async action(
          ctx,
          {
            userTypeId,
            schedulerId,
            email,
            password,
            firstname,
            lastname,
            cellNumber,
          }
        ) {
          try {
            const [userEmailExists] = await this.kx("user")
              .select("email")
              .where({ email })
              .limit(1);

            if (userEmailExists) {
              throw err(400, "Email address already exists");
            }

            const saltRounds = await genSalt(10);

            const hashedPassword = await hash(password, saltRounds);

            const [user] = await this.kx("user").insert(
              {
                email,
                password: hashedPassword,
                fkUserTypeId: userTypeId,
                fkSchedulerUserId: schedulerId,
                isActive: true,
                firstname,
                lastname,
                cellNumber,
              },
              "*"
            );

            delete user.password;
            return { user };
          } catch (error) {
            if (error.statusCode) {
              throw error;
            }
            throw err(500, "There was an error when trying to add the user.");
          }
        },
      }),
      route({
        path: "/user-types",
        method: HttpMethod.GET,
        schema: emptySchema(),
        middleware: [],
        returning: [
          {
            id: true,
            type: true,
          },
        ],
        async action(ctx) {
          try {
            const userTypeList = await this.kx("user_type").select([
              "id",
              "type",
            ]);

            return userTypeList;
          } catch (error) {
            if (error.statusCode) {
              throw error;
            }
            throw err(
              500,
              "There was an error when getting the list of user types."
            );
          }
        },
      }),
      route({
        path: "/change-user-status",
        method: HttpMethod.POST,
        schema: emptySchema().addNumber("pkUserId", {}, true, false),
        middleware: [],
        returning: [
          {
            id: true,
            isActive: true,
          },
        ],
        async action(ctx, { pkUserId }) {
          try {
            const [currentUser] = await this.kx("user")
              .select(["id", "isActive"])
              .where({
                id: pkUserId,
              })
              .limit(1);

            const newUserResults = await this.kx("user")
              .update({
                isActive: !currentUser.isActive,
              })
              .where({
                id: pkUserId,
              })
              .returning(["id", "isActive"]);

            return newUserResults;
          } catch (error) {
            if (error.statusCode) {
              throw error;
            }
            throw err(
              500,
              "There was an error changing the users active status."
            );
          }
        },
      }),
    ]);
  },
};

export default factory;
