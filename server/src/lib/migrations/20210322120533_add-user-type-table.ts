import * as Knex from 'knex';
import { baseColumns } from '../migration-helpers/base-columns';

export const up = async (knex: Knex) => {
  await knex.schema.createTable('user_type', table => {
    baseColumns(knex, table);
    table.string('type', 100).notNullable();
    table.boolean('is_admin').notNullable();
    table.boolean('is_scheduler').notNullable();
    table.boolean('is_artist').notNullable();
  });
};

export const down = async (knex: Knex) => {
  await knex.schema.dropTable('user_type');
};
