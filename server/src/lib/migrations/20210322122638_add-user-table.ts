import * as Knex from 'knex';
import { baseColumns } from '../migration-helpers/base-columns';

export const up = async (knex: Knex) => {
  await knex.schema.createTable('user', table => {
    baseColumns(knex, table);
    table.integer('fk_user_type_id')
    table.foreign('fk_user_type_id').references('user_type.id')
    table.string('email', 100).unique()
    table.text('password');
    table.boolean('is_active').notNullable()
    table.string('firstname', 100).notNullable()
    table.string('lastname', 100).notNullable()
    table.string('cell_number', 11).notNullable()
    table.string('shop_name', 100).nullable()
    table.string('address', 200).nullable()
    table.string('postal', 7).nullable()
    table.string('city', 100).nullable()
    table.string('province', 100).nullable()
  });
};

export const down = async (knex: Knex) => {
  await knex.schema.alterTable('user', table => {
    table.dropUnique(['email']);
    table.dropForeign(['fk_user_type_id']);
  });
  await knex.schema.dropTable('user');
};
