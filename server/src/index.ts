import { join } from 'path';
import * as Koa from 'koa';
import * as cors from 'koa2-cors';
import { createRouterFactories, nestedRouter } from '@lcdev/router';
import config, { loadConfig } from '@lcdev/app-config';

import { connect, Knexion, runSeeds } from './lib/database';

const isProduction = process.env.NODE_ENV === 'production';
const isStaging = process.env.NODE_ENV === 'staging';

const exitSignals = {
  SIGINT: 0,
  SIGTERM: 0,
};

async function main() {
  Object.entries(exitSignals).forEach(([signal, exitCode]) => {
    process.on(signal as NodeJS.Signals, async () => {
      console.log(`Received '${signal}'. Shutting down server.`);
      console.log('Shutting down....');
      process.exit(exitCode);
    })
  })

  await loadConfig();

  const app = new Koa();

  const knex = connect(config.database, true, Knexion.Default);

  await knex.migrate.latest();
  await runSeeds(config.database);

  const allowAllOrigins = config.webServer.allowedOrigins.find(origin => origin === '*');

  app.use(cors({
    credentials: true,
    origin: allowAllOrigins ? '*' : (ctx) => {
      const origin = ctx.get('origin');
      if (config.webServer.allowedOrigins.includes(origin)) {
        return origin;
      }
      return false;
    }
  }));

  // all routes are nested in the /api namespace
  const api = nestedRouter(join(__dirname, 'routes'), '/api');
  const router = await createRouterFactories([api]);

  app
    .use(router.routes())
    .use(router.allowedMethods())
    .use(ctx => ctx.throw(404, 'Not found'));

  app.listen(config.webServer.port, () => {
    console.log(`Server Started on Port: ${config.webServer.port}`);
  });
}

main().catch(err => {
  console.error('a fatal error occurred', err);

  process.exit(1);
});
