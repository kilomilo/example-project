module.exports = {
  extends: '@lcdev',
  parserOptions: {
    tsconfigRootDir: __dirname,
  },
  rules: {
    'no-multi-assign': 'off'
  },
};
