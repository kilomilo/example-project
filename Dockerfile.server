# App build/dependency download stage
FROM node:10-alpine as build

ARG PROJECT_NAME
RUN test -n "${PROJECT_NAME}"

WORKDIR /build

# build dependencies of typical dependencies like bcrypt
RUN apk add python make g++ autoconf automake yarn

COPY server/ .
COPY server/.app-config.meta.yml .
COPY server/.app-config.secrets.yml .
COPY server/.app-config.yml .

RUN ls -al

# Download development dependencies
RUN yarn install --frozen-lockfile --production=false

# Build project
RUN yarn build

# Remove all node_modules folders to remove dev dependencies
RUN find . -type d -name "node_modules" -prune -exec rm -r {} +

# Download production dependencies for runtime
RUN yarn install --frozen-lockfile --production

# Remove all source files that should not exist in production
RUN find . ! -type d \
  ! -name "tsconfig.json" \
  ! -name "package.json" \
  ! -name "yarn.lock" \
  ! -path "*dist*" \
  ! -path "*node_modules*" \
  ! -path "*schemas*" \
  ! -path "*schema*" \
  ! -path "*email-templates*" \
  ! -path "*seeds*" \
  ! -path "*db-snapshot*" \
  -delete

# Final image stage
FROM node:10-alpine

ARG PROJECT_NAME
ARG IMAGE_ROOT_PATH=/usr/src/app

# NODE_ENV is production unless otherwise overriden at runtime
ENV NODE_ENV production
WORKDIR $IMAGE_ROOT_PATH

# Copy files from build stage
COPY --from=build /build .
COPY server/.app-config.meta.yml .
COPY server/.app-config.secrets.yml .
COPY server/.app-config.yml .
COPY server/.app-config.meta.yml ./dist/
COPY server/.app-config.secrets.yml ./dist/
COPY server/.app-config.yml ./dist/

RUN ls -al
RUN ls -al dist/

ENV PORT 80
EXPOSE 80

CMD ["node", "./dist/index.js"]
